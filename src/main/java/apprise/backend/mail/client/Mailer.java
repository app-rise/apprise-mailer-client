package apprise.backend.mail.client;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import apprise.backend.event.EventBus;
import apprise.backend.event.EventBus.BusInstance;
import apprise.backend.mail.model.Constants;
import apprise.backend.mail.model.MailEvent;

public interface Mailer {

    public void send(MailEvent event);

    @ApplicationScoped
    public static class Default implements Mailer {

        @Inject
        BusInstance bus;

        @Produces
        static BusInstance init(EventBus eventbus) {

            return eventbus.instanceFor(Constants.mailTopic);
        }

        @Override
        public void send(MailEvent event) {
           
            bus.publish(event);
        }

    }
}
