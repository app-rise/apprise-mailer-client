package apprise.backend.mail.model;

import apprise.backend.iam.model.Tenant;

public class Constants {


    public static String mailTopic = "mail";
    public static String broadcastTarget = "all";
    public static String noTenantTarget = Tenant.notenant.id();
    public static String topicSeparator = "\\.";


}
