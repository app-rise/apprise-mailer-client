package apprise.backend.mail.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MailTarget {

   public static final String targetSeparator = ":";

   public static MailTarget broadcast = new MailTarget().value(Constants.broadcastTarget);
   public static MailTarget noTenantTarget = new MailTarget().value(Constants.noTenantTarget);

    @JsonValue
    String value;

    @JsonIgnore
    public boolean isBroadcast() {
        return this.equals(broadcast);
    }

    @JsonIgnore
    public boolean isNoTenant() {
        return this.equals(noTenantTarget);
    }

}
