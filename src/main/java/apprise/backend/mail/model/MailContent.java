package apprise.backend.mail.model;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.Dependent;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import apprise.backend.model.Multilingual;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@JsonTypeInfo(use = Id.DEDUCTION )
@JsonSubTypes({

    @Type(value = MailContent.Translated.class),
    @Type(value = MailContent.Raw.class)

})
public interface MailContent {

    MailContent extendFor(MailRecipient tenant);

    @Data
    static class Translated implements MailContent {

        Multilingual subject;

        Multilingual text;

        public MailContent intern() {
            return this;
        };

        @Override
        public MailContent extendFor(MailRecipient ignored) {
            return this;
        }
    }

    @Dependent @ToString
    static class Raw implements MailContent {

        @Getter @Setter
        String template;

        @Getter @Setter
        Map<String, Object> parameters;

        @Override
        public MailContent extendFor(MailRecipient tenant) {

            var augmentedParams = new HashMap<>(parameters);

            augmentedParams.put("targetTenant",tenant.name());
            augmentedParams.put("targetTenantId",tenant.id());

            return new Raw().template(template).parameters(augmentedParams);
        }

    }
}
