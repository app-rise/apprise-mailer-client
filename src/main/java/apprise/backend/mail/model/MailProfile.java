package apprise.backend.mail.model;

import java.util.Collection;
import java.util.HashSet;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MailProfile {

    // by default, a recipient with no profile will receive any mail.
    public static final MailProfile noProfile = new MailProfile();

    public MailProfile(MailProfile other) {

        topics(other.topics());
    }

    Collection<MailTopic> topics = new HashSet<>();


    // a profile matches a topic T (eg. 'a.b.c') if:
    // 1. the profile has no topics of its own (anyhting is accepted).
    // 2. the profile contains a topic which is more general than T (e.g. 'a.b' or 'a').
    public boolean matches(MailTopic topic) {

        return (topics.isEmpty() || topics.stream().anyMatch(t->topic.isMoreSpecificThan(t))) && !topics.contains(MailTopic.noTopic);
    }
    
}
