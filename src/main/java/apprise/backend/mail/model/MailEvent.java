package apprise.backend.mail.model;

import java.util.List;

import apprise.backend.validation.CheckDsl;
import apprise.backend.validation.Validated;
import lombok.Data;
import lombok.ToString;


@Data
@ToString(includeFieldNames = true)
public class MailEvent implements Validated {

    // dot-separated string.
    MailTopic topic;

    // wildcard (broadcast), tenant id, no tenant, user id.
    MailTarget target;

    MailContent content;

    @Override
    public List<String> validate() {

        return CheckDsl
                .given(this)
                .check($->topic!=null).or("missing topic")
                .check($->target!=null).or("missing target")
                .check($->content!=null).or("missing content")
                .andCollect();
    }

}
