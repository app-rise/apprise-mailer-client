package apprise.backend.mail.model;

import static apprise.backend.mail.model.MailProfile.noProfile;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import apprise.backend.iam.model.Privileged;
import apprise.backend.iam.model.Tenant;
import apprise.backend.iam.model.User;
import apprise.backend.model.Multilingual;
import apprise.backend.model.Multilingual.Language;
import apprise.backend.validation.CheckDsl;
import apprise.backend.validation.Validated;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Data
@NoArgsConstructor
@ToString(includeFieldNames = true)
@EqualsAndHashCode(callSuper = true)
@Slf4j
public class MailRecipient extends Privileged.Base<MailRecipient> implements Validated {

    public static enum Type {
        user, tenant
    }

    boolean active;
    String id;
    Type type;
    Multilingual name;
    Language language;
    String address;
    Set<String> additionalAddresses = new HashSet<>();
    MailRecipient tenant;
    MailProfile profile;

    public MailRecipient(MailRecipient other) {

        active(other.active)
                .id(other.id)
                .type(other.type)
                .name(other.name)
                .language(other.language)
                .address(other.address)
                .additionalAddresses(other.additionalAddresses)
                .tenant(other.tenant == null ? null : new MailRecipient(other.tenant))
                .profile(other.profile == null ? null : new MailProfile(other.profile));

    }

    public Language language() {

        // defaults to tenants
        return Optional.ofNullable(language).orElse(tenant == null ? null : tenant.language());
    }

    public String nameInCurrentLanguage() {

        return Optional.ofNullable(name).map(name -> name.in(language())).orElse(null);
    }

    public Optional<MailRecipient> fromTenant(Tenant t) {

        try {

            return Optional.of(id(t.id())
                    .active(t.lifecycle().state().equals(Tenant.State.active))
                    .type(Type.tenant)
                    .name(t.name())
                    .address(t.preferences().lookup("email", String.class).orElse(null))
                    .additionalAddresses(t.preferences().lookup("alternativeEmails", String[].class)
                            .map(Arrays::asList)
                            .map(HashSet::new)
                            .orElse(new HashSet<String>()))
                    .language(t.preferences().lookup("language", String.class).map(lang -> Language.valueOf(lang))
                            .orElse(null))
                    .profile(t.preferences().lookup("mailProfile", MailProfile.class).orElse(noProfile)));

        } catch (Throwable th) {

            log.error("can't ingest tenant ({})...", t);

            return Optional.empty();
        }

    }

    public Optional<MailRecipient> fromUser(User u) {

        try {

            MailRecipient recipient = id(u.username())
                    .active(u.lifecycle().state().equals(User.State.active))
                    .type(Type.user)
                    .name(new Multilingual().inDefaultLanguage(u.details().name()))
                    .tenant(new MailRecipient().id(u.tenant().value()))
                    .address(u.details().email())
                    .additionalAddresses(u.details().preferences().lookup("alternativeEmails", String[].class)
                            .map(Arrays::asList)
                            .map(HashSet::new)
                            .orElse(new HashSet<String>()))
                    .language(u.details().preferences().lookup("language", String.class)
                            .map(lang -> Language.valueOf(lang))
                            .orElse(null))
                    .profile(u.details().preferences().lookup("mailProfile", MailProfile.class).orElse(noProfile))
                    .permissions(u.permissions());

            if (recipient.profile() != noProfile)
                recipient.profile().topics().add(new MailTopic().value(u.username()));

            return Optional.of(recipient);

        } catch (Throwable th) {

            log.error("can't ingest user ({})...", u);


            return Optional.empty();
        }

    }

    @Override
    public List<String> validate() {
        return CheckDsl.given(this)
                .check($ -> id != null).or("missing identifier")
                .check($ -> name != null).or("missing name")
                .provided(type == Type.user).check($ -> tenant != null).or("missing tenant")
                .provided(type == Type.user).check($ -> address != null).or("missing address")
                .provided(type == Type.tenant).check($ -> tenant == null).or("unpexcted tenant")
                .andCollect();
    }

}
