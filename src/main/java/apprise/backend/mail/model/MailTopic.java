package apprise.backend.mail.model;

import static apprise.backend.mail.model.Constants.topicSeparator;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

import apprise.backend.iam.model.Action;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MailTopic {

    // the most generic topic has no labels and matches any other.
    public static final MailTopic anyTopic = new MailTopic();

    public static final MailTopic noTopic = new MailTopic().value("none");

    String value;


    Set<Action> audience = new HashSet<Action>();

    public boolean isMoreSpecificThan(MailTopic topic) {    // if it has a superset of labels.

        var labels = labels();
        var others = topic.labels();

        return labels.length >= others.length && IntStream.range(0,others.length).allMatch(i->labels[i].equals(others[i]));
    }

    public String[] labels() {
        return value == null ? new String[0] : value.split(topicSeparator);
    }

}
