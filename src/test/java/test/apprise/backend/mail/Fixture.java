package test.apprise.backend.mail;

import static apprise.backend.mail.model.MailProfile.noProfile;
import static java.lang.String.format;
import static test.apprise.backend.Fixture.someMultilingual;
import static test.apprise.backend.Testmate.any;

import java.util.Map;

import apprise.backend.mail.model.MailContent;
import apprise.backend.mail.model.MailEvent;
import apprise.backend.mail.model.MailRecipient;
import apprise.backend.mail.model.MailTarget;
import apprise.backend.mail.model.MailTopic;
import apprise.backend.model.Id;

public class Fixture {

    public static MailEvent someMailEvent() {

        return new MailEvent()

                .target(someTarget())
                .topic(someTopic())
                .content(someRawContent());
    }

    public static MailEvent someTranslatedMailEvent() {

        return new MailEvent()

                .target(someTarget())
                .topic(someTopic())
                .content(someRawContent());
    }

    public static MailRecipient someMailRecipient(String tenant) {

        return someMailRecipient().tenant(new MailRecipient().id(tenant));
     
    }
    public static MailRecipient someMailRecipient() {

        return  baseRecipient().tenant(someTenantRecipient());

    }

    public static MailRecipient someTenantRecipient() {

        return baseRecipient().id(any("tenant")).address(null).profile(noProfile).tenant(null);

    }


    public static MailRecipient baseRecipient() {

        return new MailRecipient()

                .active(true)
                .address(format("%s@acme.org", any("user")))
                .profile(noProfile)
                .id(any("recipient"))
                .name(someMultilingual(any("name")))
                .language(test.apprise.backend.Fixture.someLanguage());

    }

    public static MailTarget someTarget() {

        return new MailTarget().value(Id.mint("target"));
    }

    public static MailTopic someTopic() {

        return new MailTopic().value(Id.mint("topic"));
    }

    public static MailContent someRawContent() {

        return new MailContent.Raw().template("test").parameters((Map.of("some","param")));
    }

    public static MailContent someTranslatedContent() {

        return new MailContent.Translated()
                .subject(someMultilingual(any("subject")))
                .text(someMultilingual(any("content")));
    }
}
