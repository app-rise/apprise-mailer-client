package test.apprise.backend.mail;


import javax.inject.Inject;

import org.junit.Test;

import apprise.backend.mail.client.Mailer;
import test.apprise.backend.Testbase;

public class SmokeTest extends Testbase {
    
    @Inject
    Mailer service;

    @Test
    public void can_send_mail_events() {

        service.send(Fixture.someMailEvent());
    }
    
}